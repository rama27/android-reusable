package com.example.android_reusable_widget

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.android_reusable_widget.button.ButtonFragment
import com.example.android_reusable_widget.button.MaterialButtonFragment
import com.example.android_reusable_widget.fragment.AlertCardWarningFragment
import com.example.android_reusable_widget.fragment.CardViewFragment
import com.example.android_reusable_widget.list.RecyclerViewFragment
import com.example.android_reusable_widget.textview.TextViewFragment


class ContentActivity : AppCompatActivity() {
    private var state = ""
    private var title = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        state = intent.getStringExtra("state")!!
        initTitle()
//        initToolbar()
        initFragment(state)
    }


    private fun initTitle() {
        if (state == "card_view") {
            title = "Item Card View For RecyclerView"
        } else if (state == "alert_card_warning") {
            title = "Alert Card Warning"
        } else if (state == "rview") {
            title = "Recycler View Dummy Data"
        } else if (state == "rview") {
            title = "Recycler View Dummy Data"
        } else if (state == "filter_date_picker") {
            title = "Filter View Dummy Data"
        }
    }

    private fun initToolbar() {
        this.supportActionBar!!.title = title
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar!!.setDisplayShowHomeEnabled(true)
        this.supportActionBar!!.setBackgroundDrawable(ColorDrawable(getResources().getColor(R.color.white)))
    }


    private fun initFragment(state: String) {
        lateinit var fragment: Fragment
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()

        if (state == "card_view") {
            fragment = CardViewFragment()
        } else if (state == "alert_card_warning") {
            fragment = AlertCardWarningFragment()
        } else if (state == "rview") {
            fragment = RecyclerViewFragment()
        } else if (state == "material_button") {
            fragment = MaterialButtonFragment()
        } else if (state == "button") {
            fragment = ButtonFragment()
        } else if (state == "textview") {
            fragment = TextViewFragment()
        }else if (state == "textview") {
            fragment = TextViewFragment()
        }

        transaction.replace(R.id.frame_container, fragment, null)
        transaction.commit()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}