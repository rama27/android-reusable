package com.example.android_reusable_widget.support.utils

import android.content.Context
import android.content.SharedPreferences

class DataConfig(context: Context) {

    val TOKEN = "token"
    val USERNAME = "username"
    val PASSWORD = "password"
    val ROLE_ID = "role_id"
    val FULL_NAME = "full_name"
    val USER_NAME = "user_name"
    val USER_ID = "user_id"
    val WAREHOUSE_ID = "warehouse_id"
    val WAREHOUSE_NAME = "warehouse_name"
    val DISTRICT_ID = "district_id"
    val DISTRICT_NAME = "district_name"
    val PHONE = "phone"
    val PROFILE_PHOTO = "profile_photo"
    val STATE_FRAGMENT = "state_fragment"

    private val SP_CONFIG = "posko_pelayanan"
    val STATUS_ID = "status_id"


    val FILTER_START_DATE = "FILTER_START_DATE"
    val FILTER_END_DATE = "FILTER_END_DATE"



    private var sp: SharedPreferences =
        context.getSharedPreferences(SP_CONFIG, Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = sp.edit()

    fun setLogin() {
        editor.putBoolean("login", true)
        editor.commit()
    }

    fun isLogin(): Boolean {
        return sp.getBoolean("login", false)
    }

    fun storeString(key: String, data: String?) {
        if (data != null) {
            editor.putString(key, data)
        } else {
            editor.putString(key, "")
        }
        editor.commit()
    }

    fun getString(key: String): String {
        return sp.getString(key, "")!!
    }

    fun clearAll() {
        editor.clear()
        editor = sp.edit()
        editor.clear()
        editor.commit()
    }
}