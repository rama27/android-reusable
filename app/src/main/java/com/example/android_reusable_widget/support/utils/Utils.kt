package com.example.android_reusable_widget.support.utils

import android.app.DatePickerDialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.text.TextUtils
import android.util.TypedValue
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
//import co.id.energeek.dpubmpposkoperalatan.model.response.general.ErrorException
//import com.google.gson.Gson
//import com.google.gson.reflect.TypeToken
//import okhttp3.MediaType
//import okhttp3.MultipartBody
//import okhttp3.RequestBody
//import okhttp3.ResponseBody
import java.io.File
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    //datetime
    val currentYear: Int
        get() {
            val year = Calendar.getInstance().get(Calendar.YEAR)
            return year
        }

    val currentMonth: Int
        get() {
            val month = Calendar.getInstance().get(Calendar.MONTH)
            return month
        }

    val currentDay: Int
        get() {
            val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            return day
        }

    fun convertDate(date: String?, df1: SimpleDateFormat, df2: SimpleDateFormat): String {
        var date = date
        try {
            val value = df1.parse(date)
            date = df2.format(value)
        } catch (e: Exception) {
            date = date
        }
        return date!!
    }

    fun monthName(month: Int): String {
        var bulan = ""
        if (month == 1) {
            bulan = "Januari"
        } else if (month == 2) {
            bulan = "Februari"
        } else if (month == 3) {
            bulan = "Maret"
        } else if (month == 4) {
            bulan = "April"
        } else if (month == 5) {
            bulan = "Mei"
        } else if (month == 6) {
            bulan = "Juni"
        } else if (month == 7) {
            bulan = "Juli"
        } else if (month == 8) {
            bulan = "Agustus"
        } else if (month == 9) {
            bulan = "September"
        } else if (month == 10) {
            bulan = "Oktober"
        } else if (month == 11) {
            bulan = "November"
        } else if (month == 12) {
            bulan = "Desember"
        }
        return bulan
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun isEmpty(ed: EditText): Boolean {
        val temp = ed.text.toString().replace("\\s".toRegex(), "")
        return TextUtils.isEmpty(temp)
    }

    fun getString(ed: EditText): String {
        return ed.text.toString()
    }

    fun dpToPx(dp: Float, context: Context): Int {
        return dpToPx(
            dp,
            context.resources
        )
    }

    private fun dpToPx(dp: Float, resources: Resources): Int {
        val px =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
        return px.toInt()
    }



    fun showDatePickerWithTextView(date_input: String, context: Context,  textview: TextView) {
        var date_selected = ""

        if (date_input.isEmpty()) {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            var dpd = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    var date: String =
                        dayOfMonth.toString() + "-" + (monthOfYear + 1).toString() + "-" + year.toString()
                    date =
                        convertDate(
                            date,
                            SimpleDateFormat("dd-M-yyyy"),
                            SimpleDateFormat("dd-MM-yyyy")
                        )
                    date_selected = date
                    textview.text = date_selected
                },
                year,
                month,
                day
            )

            dpd.show()
        } else {
            val date = date_input
            val calend = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val day = Integer.parseInt(calend[0])
            val month = Integer.parseInt(calend[1])
            val year = Integer.parseInt(calend[2])

            var dpd = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    var date: String =
                        dayOfMonth.toString() + "-" + (monthOfYear + 1).toString() + "-" + year.toString()
                    date =
                        convertDate(
                            date,
                            SimpleDateFormat("dd-M-yyyy"),
                            SimpleDateFormat("dd-MM-yyyy")
                        )
                    date_selected = date
                    textview.text = date_selected
                },
                year,
                month - 1,
                day
            )
            dpd.show()
        }
    }

    fun showDatePicker(context: Context){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        var dpd = DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                var date: String =
                    dayOfMonth.toString() + "-" + (monthOfYear + 1).toString() + "-" + year.toString()
                date =
                    convertDate(
                        date,
                        SimpleDateFormat("dd-M-yyyy"),
                        SimpleDateFormat("dd-MM-yyyy")
                    )
            },
            year,
            month,
            day
        )

        dpd.show()
    }

//    fun errorHandler(body: ResponseBody?): String {
//        val gson = Gson()
//        val type = object : TypeToken<ErrorException>() {}.type
//        var errorResponse: ErrorException = gson.fromJson(body!!.charStream(), type)
//        return errorResponse.status.message!!
//    }


//    fun jsonRegex(json: String): String {
//        return json.replace("\n", "").replace("\r", "")
//    }

//    fun fileMultipart(path: String, key: String): MultipartBody.Part {
//        var fileUpload = File(path)
//        var filePart = MultipartBody.Part.createFormData(
//            "${key}",
//            fileUpload?.getName(),
//            RequestBody.create(
//                MediaType.parse("image/*"), fileUpload
//            )
//        )
//        return filePart
//    }

    fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isEnabled = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.setBackgroundColor(Color.TRANSPARENT)
    }

    fun NumbertoCurrency(number: Int): String {
        return NumberFormat.getNumberInstance(Locale.US).format(number).replace(',', '.');
    }


    fun getDecimalFormattedString(value: String): String? {
        val lst = StringTokenizer(value, ".")
        var str1 = value
        var str2 = ""
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken()
            str2 = lst.nextToken()
        }
        var str3 = ""
        var i = 0
        var j = -1 + str1.length
        if (str1[-1 + str1.length] == '.') {
            j--
            str3 = "."
        }
        var k = j
        while (true) {
            if (k < 0) {
                if (str2.length > 0) str3 = "$str3.$str2"
                return str3
            }
            if (i == 3) {
                str3 = ",$str3"
                i = 0
            }
            str3 = str1[k].toString() + str3
            i++
            k--
        }
    }

    fun trimCommaOfString(string: String): String? {
//        String returnString;
        return if (string.contains(",")) {
            string.replace(",", "")
        } else {
            string
        }
    }
}