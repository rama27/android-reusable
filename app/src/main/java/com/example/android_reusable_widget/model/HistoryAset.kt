package com.example.android_reusable_widget.model

data class HistoryAsset(val old_location: String?, val new_location: String?, val date_transfer: String?)