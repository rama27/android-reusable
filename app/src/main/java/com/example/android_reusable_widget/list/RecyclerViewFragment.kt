package com.example.android_reusable_widget.list

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_reusable_widget.R
import com.example.android_reusable_widget.model.HistoryAsset
import kotlinx.android.synthetic.main.fragment_recycler_view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RecyclerViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RecyclerViewFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var mAdapter: RecyclerViewAdapter
    private var mList: ArrayList<HistoryAsset> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        initDataDummyList()
    }

    private fun initRecycler() {
        mAdapter = RecyclerViewAdapter(context!!)
        rv_list.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        rv_list.layoutManager = layoutManager
        rv_list.adapter = mAdapter
    }

    private fun initDataDummyList() {
        mList.add(HistoryAsset("PA Romo Kalisaro","PA Kedung Asem","10 Februari 2021"))
        mList.add(HistoryAsset("PA Tandes","PA Wiyung","13 Februari 2021"))
        mList.add(HistoryAsset("PA Romo Kalisaro","PA Kedung Asem","10 Februari 2021"))
        mList.add(HistoryAsset("PA Tandes","PA Wiyung","13 Februari 2021"))
        mList.add(HistoryAsset("PA Romo Kalisaro","PA Kedung Asem","10 Februari 2021"))
        mList.add(HistoryAsset("PA Tandes","PA Wiyung","13 Februari 2021"))
        mList.add(HistoryAsset("PA Romo Kalisaro","PA Kedung Asem","10 Februari 2021"))
        mList.add(HistoryAsset("PA Tandes","PA Wiyung","13 Februari 2021"))
        Log.d("list",mList.size.toString())
        mAdapter.updateData(mList)
        mAdapter.notifyDataSetChanged()
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RecyclerViewFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RecyclerViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}