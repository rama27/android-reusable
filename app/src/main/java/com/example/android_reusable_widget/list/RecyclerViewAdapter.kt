package com.example.android_reusable_widget.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android_reusable_widget.R
import com.example.android_reusable_widget.model.HistoryAsset
import kotlinx.android.synthetic.main.item_card_view.view.*

class RecyclerViewAdapter(private val mContext: Context) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private var dataList: List<HistoryAsset> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = dataList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position], position, mContext)
    }

    fun updateData(dataList: List<HistoryAsset>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: HistoryAsset, position: Int, ctx: Context) {

            itemView.tv_tanggal_perpindahan.text = item.date_transfer
            itemView.tv_lokasi_awal.text =  item.old_location
            itemView.tv_lokasi_akhir.text = item.new_location

            itemView.setOnClickListener {
//                val intent = Intent(
//                    ctx,
//                    DetailAlatBeratActivity::class.java
//                )
//                intent.putExtra("item", item)
//                ctx.startActivity(intent)
            }


        }
    }
}