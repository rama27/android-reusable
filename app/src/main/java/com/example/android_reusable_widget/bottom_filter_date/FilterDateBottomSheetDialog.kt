package com.example.android_reusable_widget.bottom_filter_date

import android.annotation.SuppressLint
import android.content.Context

import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewTreeObserver
import com.example.android_reusable_widget.MainActivity
import com.example.android_reusable_widget.R
import com.example.android_reusable_widget.support.utils.DataConfig
import com.example.android_reusable_widget.support.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_filter_date.*

/**
 * A simple [Fragment] subclass.
 */
class FilterDateBottomSheetDialog(
    context: Context,
    activity: MainActivity
) : BottomSheetDialog(context), View.OnClickListener {

    lateinit var bottomSheetView: View
    private lateinit var dataConfig: DataConfig
    private var mView = activity

    init {
        create()
    }

    override fun create() {
        bottomSheetView = layoutInflater.inflate(R.layout.bottom_filter_date, null)
        setContentView(bottomSheetView)
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView.parent as View)

        bottomSheetView.getViewTreeObserver()
            .addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    bottomSheetView.getViewTreeObserver().removeGlobalOnLayoutListener(this)
                    val height = bottomSheetView.getMeasuredHeight()
                    bottomSheetBehavior.peekHeight = height
                }
            })

        val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // do something
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // do something
            }
        }

        initVar()
        initClick()
        initView()
    }


    private fun initVar() {
        dataConfig = DataConfig(context)
    }

    private fun initView() {
        if (dataConfig.getString(dataConfig.FILTER_START_DATE).isNotEmpty())
            tv_start_date.text = dataConfig.getString(dataConfig.FILTER_START_DATE)

        if (dataConfig.getString(dataConfig.FILTER_END_DATE).isNotEmpty())
            tv_end_date.text = dataConfig.getString(dataConfig.FILTER_END_DATE)
    }

    private fun initClick() {
        btnTerapkan.setOnClickListener(this)
        btnReset.setOnClickListener(this)
        tv_start_date.setOnClickListener(this)
        tv_end_date.setOnClickListener(this)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnTerapkan -> {
                if (tv_start_date.text.toString() != "Pilih Tanggal")
                    dataConfig.storeString(
                        dataConfig.FILTER_START_DATE,
                        tv_start_date.text.toString()
                    )

                if (tv_end_date.text.toString() != "Pilih Tanggal")
                    dataConfig.storeString(
                        dataConfig.FILTER_END_DATE,
                        tv_end_date.text.toString()
                    )

                //hit api
                hide()
            }

            R.id.btnReset -> {
                dataConfig.storeString(dataConfig.FILTER_START_DATE, "")
                dataConfig.storeString(dataConfig.FILTER_END_DATE, "")

                //hit api
                hide()
            }

            R.id.tv_start_date -> {
                if (tv_start_date.text == "Pilih Tanggal" || tv_start_date.text == "")
                    Utils.showDatePickerWithTextView("", context, tv_start_date)
                else
                    Utils.showDatePickerWithTextView(
                        tv_start_date.text.toString(),
                        context,
                        tv_start_date
                    )
            }

            R.id.tv_end_date -> {
                if (tv_end_date.text == "Pilih Tanggal" || tv_end_date.text == "")
                    Utils.showDatePickerWithTextView("", context, tv_end_date)
                else
                    Utils.showDatePickerWithTextView(tv_end_date.text.toString(), context, tv_end_date)
            }
        }
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        private val INSTANCE: FilterDateBottomSheetDialog? = null

        fun getInstance(
            context: Context,
            activity: MainActivity
        ): FilterDateBottomSheetDialog {
            return INSTANCE ?: FilterDateBottomSheetDialog(context, activity)
        }
    }


}