package com.example.android_reusable_widget

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.android_reusable_widget.bottom_filter_date.FilterDateBottomSheetDialog
import com.example.android_reusable_widget.support.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initClick()
    }

    private fun initClick() {
        val intent = Intent(
            this,
            ContentActivity::class.java
        )
        btnCardView.setOnClickListener {
            intent.putExtra("state", "card_view")
            startActivity(intent)
        }
        btnAlertCard.setOnClickListener {
            intent.putExtra("state", "alert_card_warning")
            startActivity(intent)
        }
        btnSheetDialogWarning.setOnClickListener {
            //show bottom sheet
            val bottomSheetDialogFragment = BottomSheetWarning()
            bottomSheetDialogFragment.show(supportFragmentManager!!, "Dialog")
        }
        btnRecyclerView.setOnClickListener {
            intent.putExtra("state", "rview")
            startActivity(intent)
        }

        btnCustomMaterialButton.setOnClickListener {
            intent.putExtra("state", "material_button")
            startActivity(intent)
        }

        btnCustomButton.setOnClickListener {
            intent.putExtra("state", "button")
            startActivity(intent)
        }

        btnCustomTextView.setOnClickListener {
            intent.putExtra("state", "textview")
            startActivity(intent)
        }

        btnDatePicker.setOnClickListener {
            Utils.showDatePicker(this)
        }

        btnFilterDatePicker.setOnClickListener {
            val myBottomSheetDialog = FilterDateBottomSheetDialog.getInstance(this, this)
            myBottomSheetDialog.show()
        }
    }


}